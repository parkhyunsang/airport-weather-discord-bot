# Airport Weather Discord Bot
Airport Weather은 비행 시뮬레이션과 실제 조종사를 직업으로 하시고 계신 분들에게 편하게 날씨를 알려줄 수 있는 봇입니다.  
많은 사람들에게 편하고 안전한 비행을 할 수 있도록 도와 주고 있으며 재밌는 게임과 비행을 할 수 있도록 도와주는 프로그램입니다.  
Airport Weather is a robot that can help inform flight simulations and weather conditions for those of you who are actually pilots.   
It helps many people fly comfortably and safely, and it is a program that helps them play fun games.  
## 개발(Development)
*  node.js 
*  JavaScript
*  Discord.js
```bash
$ npm install discord.js

$ npm install axios
$ yarn add axios
$ bower install axios

$ npm install xml2js
$ bower install xml2js
```
## 기능(Function)
*  한국 공항 간 공항 비행 경로 기능(Airport flight path functionality between airports)
*  한국 공항 ICAO 코드 검색 기능(Airport ICAO Code Search Functions) 

## 필요한 파일 (API / JSON)
[Airport Weather & ICAO & API SERVER LINK](./api.json)